#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 19:50:23 2020

@author: ramkeerthyathinarayanan
"""

import array

outputFile = open('association rule.txt', 'w')

products = []
infile = open('products.csv', 'r')
for product in infile:
    products.append(product.strip().split(',')[1:])
        
def diff(first, second):
    second = set(second)
    return [item for item in first if item not in second]

def CandidateGeneration(itemset, k):
    #print(k)
    itemDict = {}
    for item1 in itemset:
        for item2 in itemset:
            listItems = set()
            
            if(len(item1 & item2) != k):
                listItems = item1 | item2
                #print('unsorted', listItems)
                listItems = sorted(listItems)
                #print('sorted', listItems)
            else:
                break
                
            if frozenset(listItems) not in itemDict.keys():
                itemDict[frozenset(listItems)] = [0, 0.0, 0.0]  
                #print('unsorted frozenset', frozenset(listItems))
                #print('sorted frozenset', sorted(frozenset(listItems)))
                
    for key in itemDict:
        keySize = len(key)
        
        for transaction in transactionSet:
            presentCount = 0;
            
            for value in key:
                if value in transaction:
                    presentCount += 1
                    
            if(keySize == presentCount):    
                itemDict[key][0] += 1
                
    delete = [key for key in itemDict if itemDict[key][0] == 0]
    for key in delete: del itemDict[key]
    
    for key in itemDict:
        itemDict[key][1] = itemDict[key][0] / len(T)
        itemSet = '{'
        
        for index, item in enumerate(key):
            itemSet += products[item][0]
            if(index != len(key) - 1):
                itemSet += ', '
                
        itemSet += '}: sup = ' +  str(itemDict[key][1]) + '\n'
            
        if(itemDict[key][1] >= 0.025):
            #print(itemSet)
            outputFile.write(itemSet)
    
    delete = [key for key in itemDict if itemDict[key][1] < 0.025]
    for key in delete: del itemDict[key] 
    
    for key in itemDict:
        masterItemsetDict[key] = itemDict[key]
        
    out = '------------------------------------------\n'
    out += 'Association Result\n'
    out += '------------------------------------------\n'
    outputFile.write(out)
    
    # Compute conf
    for key in itemDict:
        set1 = []
        sortedSetArray = sorted(set(key))
        #print('sortedSetArray', sortedSetArray)
        
        for index, item in enumerate(sortedSetArray):
            if(index < len(sortedSetArray) - 1):    
                set1.append(item)
                #print('set1', set1)
                set2 = diff(sortedSetArray, set1)
                #print('set2', set2)
                set1Count = masterItemsetDict[frozenset(set1)][0]
                conf = itemDict[key][0] / set1Count
                
                if(conf >= 0.5):
                    out = '{'
                    for index, item in enumerate(set1):
                        out += products[item][0]
                        if(index != len(set1) - 1):
                            out += ', '
                    out += '} ---> {'
                    
                    for index, item in enumerate(set2):
                        out += products[item][0]
                        if(index != len(set2) - 1):
                            out += ', '
                    out += '}: conf = ' + str(conf) + ', sup=' + str(itemDict[key][1]) + '\n'
                    
                    outputFile.write(out)

    return itemDict


fileNames = ['1000', '5000', '20000', '75000']
    
for fileName in fileNames:
    
    out = '####################\n'
    out += fileName + ' Transactions\n'
    out += '####################\n'
    outputFile.write(out)
    
    k = 1
    
    T = []
    file = fileName + '/' + fileName + '-out1.csv'
    infile = open(file, 'r')
    for line in infile:
        T.append(line.strip().split(',')[1:])
    
    itemset = []
    for index in range(50):
        itemset.append(set([index]))
    
    itemsetCount = array.array('i', (0 for i in range(0,50)))
    support = array.array('f', (0 for i in range(0,50)))
    
    transactionSet = []
    
    for transaction in T:
        itemsList = []
        
        for item in transaction:
            itemsetCount[int(item)] += 1
            itemsList.append(int(item))
            
        transactionSet.append(set(itemsList))
        
    out = '====================================\n' 
    out += 'Frequent 1-itemset is\n'
    out += '====================================\n' 
    outputFile.write(out)
            
    masterItemsetDict = {}
    
    for index, item in enumerate(itemsetCount):
        support[index] = item / len(T)
        
        if (support[index] < 0.025):
            # Delete the entity
            #print(item)
            break
        else:
            masterItemsetDict[frozenset([index])] = [item, support[index], 0.0] 
            out = str('{' + products[index][0] + '}: ' + 'sup = ' + str(support[index]) + '\n')
            #print(out)
            outputFile.write(out)
    
    out = '====================================\n' 
    out += 'Frequent 2-itemset is\n'
    out += '====================================\n' 
    outputFile.write(out)
        
    itemDict = CandidateGeneration(itemset, k)
    
    for key in itemDict:
        masterItemsetDict[key] = itemDict[key]
    
    while itemDict != {}:
        k += 1
        
        out = '====================================\n' 
        out += 'Frequent' + str(k+1) + '-itemset is\n'
        out += '====================================\n' 
        outputFile.write(out)
        
        newItemset = []
        
        for key in itemDict:
            newItemset.append(set(key))
        
        itemDict = CandidateGeneration(newItemset, k)


outputFile.close()

        